import os
import numpy as np
import scipy.optimiza.differential_evolution
os.system('ocean -restore main.ocn')

#objective = np.loadtxt('objective', skiprows=2)
#print(objective)

differential_evolution(func, 

bounds = []
# cf
bounds.append((1e-15, 10e-12))
# ibias
bounds.append((1e-9, 1e-3))
# ln
bounds.append((350e-9, 10e-6))
# lnp
bounds.append((350e-9, 10e-6))
# lp
bounds.append((350e-9, 10e-6))
# rf
bounds.append((0, 100e3))
# wnm
bounds.append((1e-6, 200e-6))
# wnp
bounds.append((1e-6, 200e-6))
# wnr
bounds.append((1e-6, 200e-6))
# wns
bounds.append((1e-6, 200e-6))
# wpm
bounds.append((1e-6, 200e-6))
# wps
bounds.append((1e-6, 200e-6))
