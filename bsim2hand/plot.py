import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# Font setting and figure initialization
matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()
ax1 = fig.add_subplot(111)
#ax1 = plt.subplot(111)

# Plot s21
arr = np.loadtxt("hand_gm.data", skiprows=2)
ax1.plot(np.transpose(arr)[0], np.transpose(arr)[1]*1E6, '-', color='red', label='gm')
arr = np.loadtxt("hand_gmb.data", skiprows=2)
ax1.plot(np.transpose(arr)[0], np.transpose(arr)[1]*1E6, '-', color='blue', label='gmb')
arr = np.loadtxt("hand_cgs.data", skiprows=2)
ax1.plot(np.transpose(arr)[0], np.transpose(arr)[1]*1E17, '-', color='green', label='cgs')
arr = np.loadtxt("hand_cgd.data", skiprows=2)
ax1.plot(np.transpose(arr)[0], np.transpose(arr)[1]*1E17, '-', color='purple', label='cgd')
arr = np.loadtxt("hand_cdb.data", skiprows=2)
ax1.plot(np.transpose(arr)[0], np.transpose(arr)[1]*1E17, '-', color='black', label='cdb')
arr = np.loadtxt("hand_cgb.data", skiprows=2)
ax1.plot(np.transpose(arr)[0], np.transpose(arr)[1]*1E17, '-', color='orange', label='cgb')
arr = np.loadtxt("hand_csb.data", skiprows=2)
ax1.plot(np.transpose(arr)[0], np.transpose(arr)[1]*1E17, '-', color='grey', label='csb')
#plt.text(2E5, 16.1, 'Midband s21=15.9(dB)', fontsize=13)

# Plot s11

ax1.set_xlabel("Vgs(V)")
ax1.set_ylabel("Transconductance(uS)")

ax2 = ax1.twinx()
ax2.set_ylabel("Capacitance(1e-17F)")

ax1.legend(prop={'size':13})
#plt.legend(loc=(0.05,0.05))

fig.set_size_inches(9, 6, forward=True)
plt.grid()
plt.savefig('sse.png', bbox_inches='tight')
plt.show()
