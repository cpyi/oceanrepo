import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# Font setting and figure initialization
matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()

# Plot s21
arr = np.loadtxt("rslt_21.dta", skiprows=2)

plt.semilogx(np.transpose(arr)[0], np.transpose(arr)[1], '-', color='black', label='s21')
plt.text(2E5, 16.1, 'Midband s21=15.9(dB)', fontsize=13)
plt.text(3201, 8, '-3dB at 3201Hz', fontsize=13)
plt.text(2E7, 8, '-3dB at 963MHz', fontsize=13)

# Plot s11
arr = np.loadtxt("rslt_11.dta", skiprows=2)

plt.semilogx(np.transpose(arr)[0], np.transpose(arr)[1], linestyle='dashdot', color='black', label='s11')

# Plot s22
arr = np.loadtxt("rslt_22.dta", skiprows=2)

plt.semilogx(np.transpose(arr)[0], np.transpose(arr)[1], linestyle='dashed', color='black', label='s22')

# Plot s21 reference line
ref = np.empty(len(np.transpose(arr)[1]))
ref.fill(15.9-3)
plt.semilogx(np.transpose(arr)[0], ref, linestyle='dotted', color='black')

# Plot s11 and s11 reference line
ref = np.empty(len(np.transpose(arr)[1]))
ref.fill(-20)
plt.semilogx(np.transpose(arr)[0], ref, linestyle='dotted', color='black')

plt.text(3E4, -15, 's11 < -20dB from 16.1KHz to 726MHz', fontsize=13)
plt.text(3E4, -19, 's22 < -20dB from 16.1KHz to 1.38GHz', fontsize=13)

plt.xlabel("Frequency(Hz)")
plt.ylabel("s-parameter(dB)")
plt.legend(loc=(0.05,0.05))

fig.set_size_inches(9, 6, forward=True)
plt.grid()
plt.savefig('sp.png', bbox_inches='tight')
plt.show()
