import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# matplotlib.rc('font', family='serif')
#matplotlib.rcParams.update({'font.size': 14})

ax = plt.subplot(111, projection='polar')
arr = np.loadtxt("amp.data", skiprows=3)
plt.plot(np.transpose(arr)[0]/180.0*3.14159, np.transpose(arr)[1])
# ax.set_rmax(2.0)
ax.grid(True)


plt.xlabel("time(ns)")
plt.ylabel("Vo(V)")

#plt.text(34, 0.5, '$\theta=theta$', fontsize=13)

#fig.set_size_inches(9, 6, forward=True)
plt.grid()
#plt.savefig('dc.png', bbox_inches='tight')
plt.show()
