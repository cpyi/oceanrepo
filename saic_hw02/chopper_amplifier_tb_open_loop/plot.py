import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()

arr = np.loadtxt("ac_db20.data", skiprows=2)
plt.semilogx(np.transpose(arr)[0], np.transpose(arr)[1], '-', color='#aaaaaa')

arr = np.loadtxt("ac_phase.data", skiprows=2)
plt.semilogx(np.transpose(arr)[0], np.transpose(arr)[1], '-', color='black')


plt.xlabel("Time(ms)")
plt.ylabel("Vo(mV)")

fig.set_size_inches(9, 6, forward=True)
plt.grid()
plt.savefig('tran_noise_100u.png', bbox_inches='tight')
plt.show()
