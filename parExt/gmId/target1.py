import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()
ax1 = fig.add_subplot(111)

arr = np.loadtxt("target1.data", skiprows=2)

w = 5e-6
l = 1e-6

id = np.transpose(arr)[0] * 1e6
gm = np.transpose(arr)[1] * 1e6
vgs = np.transpose(arr)[2]
vdsat = np.transpose(arr)[3]

x = id/(w/l)
y = gm/id

ax1.semilogx(x, y, '-', color='red')

ax1.set_xlabel("Id/(W/L)(uA)")
ax1.set_ylabel("gm/Id(uS/uA)")

ax2 = ax1.twinx()
x = id/(w/l)
y = vdsat
ax2.semilogx(x, y, '-', color='blue')
y = vgs
ax2.semilogx(x, y, '-', color='green')

ax2.set_ylabel("vdsat(V")

fig.set_size_inches(9, 6, forward=True)

ax1.grid(which='major')
ax1.grid(which='minor')
#plt.savefig('dc.png', bbox_inches='tight')
plt.show()
