import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

arr = np.loadtxt("rslt.dta", skiprows=2)

fig = plt.figure()
plt.semilogx(np.transpose(arr)[0], np.transpose(arr)[1], '-', color='black')
plt.text(2E5, 23, 'Midband Gain=21.9(dB)', fontsize=13)
plt.text(3201, 17.2, 'Low 3dB at 3201Hz', fontsize=13)
plt.text(7E6, 17.2, 'High 3dB at 963MHz', fontsize=13)

ref = np.empty(len(np.transpose(arr)[1]))
ref.fill(21.9-3)
plt.semilogx(np.transpose(arr)[0], ref, '--', color='black')

plt.xlabel("Frequency(Hz)")
plt.ylabel("Gain(dB)")

fig.set_size_inches(9, 6, forward=True)
plt.grid()
plt.savefig('hw01_tc.png', bbox_inches='tight')
plt.show()
