import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()

for i in range(-5, 6, 1):
    if i == 4:
        ls = 'solid'
    else:
        ls = 'dotted'
    arr = np.loadtxt("tran_de" + str(i*18), skiprows=2)
    plt.plot(np.transpose(arr)[0]*1e9, np.transpose(arr)[1], linestyle=ls, color='black')

plt.xlabel("time(ns)")
plt.ylabel("Vif(V)")

#plt.text(34, 0.5, '$\theta=theta$', fontsize=13)

fig.set_size_inches(9, 6, forward=True)
plt.grid()
plt.savefig('theta.png', bbox_inches='tight')
plt.show()
