import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# Font setting and figure initialization
matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()
ax1 = fig.add_subplot(111)
#ax1 = plt.subplot(111)

# Plot s21
arr = np.loadtxt("gmc_ratio_w_10u_hello.data", skiprows=2)
x = np.transpose(arr)[0]
y = np.transpose(arr)[1]
ax1.plot(x, y, '-', color='red')

val = np.interp(1, x, y)

print(val)
#plt.text(2E5, 16.1, 'Midband s21=15.9(dB)', fontsize=13)

# Plot s11

ax1.set_xlabel("Vgs(V)")
ax1.set_ylabel("gm/c(rad/s)")

ax1.legend(prop={'size':13})
#plt.legend(loc=(0.05,0.05))

fig.set_size_inches(9, 6, forward=True)
plt.grid()
plt.savefig('sse.png', bbox_inches='tight')
plt.show()
