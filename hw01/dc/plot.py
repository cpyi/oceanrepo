import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()

arr = np.loadtxt("rslt_fbk.dta", skiprows=2)
plt.plot(np.transpose(arr)[0], np.transpose(arr)[1], '-', color='black')

plt.xlabel("Vi(V)")
plt.ylabel("Vo(V)")

x = [1.61]
y = [1.61]
plt.scatter(x, y, color='black')
plt.text(34, 0.5, 'Solid line is $\theta_{1}=\theta_{2}=50 degree$', fontsize=13)

fig.set_size_inches(9, 6, forward=True)
plt.grid()
plt.savefig('dc.png', bbox_inches='tight')
plt.show()
