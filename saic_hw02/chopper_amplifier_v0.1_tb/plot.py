import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()

arr = np.loadtxt("noise.data", skiprows=2)
x = np.transpose(arr)[0]/1e3
y = np.transpose(arr)[1]*1e9
plt.plot(x, y, linestyle='solid', color='red')

arr = np.loadtxt("pnoise.data", skiprows=2)
x = np.transpose(arr)[0]/1e3
y = np.transpose(arr)[1]*1e9
plt.plot(x, y, linestyle='solid', color='blue')

plt.xlabel("Frequency(KHz)")
plt.ylabel("Noise current(nA/sqrt(H))")

plt.xlim(-20, x[-1]+20)

fig.set_size_inches(9, 6, forward=True)
plt.grid()
#plt.savefig('dc.png', bbox_inches='tight')
plt.show()
