import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()

arr = np.loadtxt("tran_noise_50m_chop_freq_1Hz_sig_amp_1m.data", skiprows=2)
plt.plot(np.transpose(arr)[0] * 1e3, np.transpose(arr)[1] * 1e3, '-', color='#aaaaaa')

arr = np.loadtxt("tran_noise_50m_chop_freq_100K_sig_amp_1m.data", skiprows=2)
plt.plot(np.transpose(arr)[0] * 1e3, np.transpose(arr)[1] * 1e3, '-', color='black')


plt.title("Transient Noise Simulation with/without Chopper")
plt.xlabel("Time(ms)")
plt.ylabel("Vo(mV)")

fig.set_size_inches(9, 6, forward=True)
plt.grid()
plt.savefig('tran_noise_1m.png', bbox_inches='tight')
plt.show()
