import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 13})

fig = plt.figure()
ax = plt.subplot(111, projection='polar')
arr = np.loadtxt('amp.data')
angle = np.transpose(arr)[0]/180*3.14159
voltage = np.transpose(arr)[1]
ax.plot(angle, voltage, color='black')
ax.grid(True)

plt.xlabel(r'$\theta_{2}(Degree)$')
plt.ylabel("Vif(V)")

ax.set_title(r'8-elements phased array simulation with $\theta_{1}=50^{\circ}$')
#ax.set_title(r'$\theta_{1}=50^{\circ}$')

#plt.text(34, 0.5, '$\theta=theta$', fontsize=13)

fig.set_size_inches(9, 6, forward=True)
plt.savefig('8e.png', bbox_inches='tight')
plt.show()
