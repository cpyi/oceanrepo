import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()
"""
for i in range(4):
    arr = np.loadtxt("Fit_sp_eng.txt", skiprows=2)
    plt.plot(np.transpose(arr)[0], np.transpose(arr)[i], '-', color='black')

for i in range(4):
    arr = np.loadtxt("Target_sp_eng.txt", skiprows=2)
    plt.plot(np.transpose(arr)[0], np.transpose(arr)[i], '-', color='red')
"""
#arr = np.loadtxt("Fit_sp_eng.txt", skiprows=2)
#plt.plot(np.transpose(arr)[0], 20*np.log10(np.transpose(arr)[3]), '-', color='black')
#arr = np.loadtxt("Target_sp_eng.txt", skiprows=2)
#plt.plot(np.transpose(arr)[0], 20*np.log10(np.transpose(arr)[3]), '-', color='black')

arr = np.loadtxt("Fit_sp_eng.txt", skiprows=2)
plt.plot(np.transpose(arr)[0]*1E-9, 20*np.log10(np.transpose(arr)[3]), linestyle="dashed", color='black', label="Fitting S21")
arr = np.loadtxt("Target_sp_eng.txt", skiprows=2)
plt.plot(np.transpose(arr)[0]*1E-9, 20*np.log10(np.transpose(arr)[3]), '-', color='black', label="Target S21")

#arr = np.loadtxt("Fit_sp_eng.txt", skiprows=2)
#plt.plot(np.transpose(arr)[0], 20*np.log10(np.transpose(arr)[3])-60, '-', color='blue', label="Fitting S21")
#arr = np.loadtxt("Target_sp_eng.txt", skiprows=2)
#plt.plot(np.transpose(arr)[0], 20*np.log10(np.transpose(arr)[3])-60, '.', color='blue', label="Target S21")

"""
arr = np.loadtxt("Fit_sp_eng.txt", skiprows=2)
plt.plot(np.transpose(arr)[0], 20*np.log10(np.transpose(arr)[3])-60, '-', color='green')
arr = np.loadtxt("Target_sp_eng.txt", skiprows=2)
plt.plot(np.transpose(arr)[0], 20*np.log10(np.transpose(arr)[3])-60, '.', color='green')

arr = np.loadtxt("Fit_sp_eng.txt", skiprows=2)
plt.plot(np.transpose(arr)[0], 20*np.log10(np.transpose(arr)[4]), '-', color='purple')
arr = np.loadtxt("Target_sp_eng.txt", skiprows=2)
plt.plot(np.transpose(arr)[0], 20*np.log10(np.transpose(arr)[4]), '.', color='purple')
"""

plt.xlim(1, 60)
plt.ylim(-110, 0)

plt.xlabel("Frequency(GHz)")
plt.ylabel("S21(dB)")

#x = [1.61]
#y = [1.61]
#plt.scatter(x, y, color='black')
plt.text(15, -56, 'Fitting result does not match the target response.', fontsize=13)
plt.legend()

fig.set_size_inches(9, 6, forward=True)
plt.grid()
plt.savefig('w_s21.png', bbox_inches='tight')
plt.show()
