import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()

fn = ["cap_5x5.data", "cap_5x1.data", "cap_1x5.data"]
label = ["5x5", "5x1", "1x5"]
ls = ["dotted", "dashed", "dashdot"]
w = [5, 5, 1]
l = [5, 1, 5]

for i in range(len(fn)):
    arr = np.loadtxt(fn[i], skiprows=3)
    data = np.transpose(arr)
    plt.plot(data[0], data[1]/(w[i]*l[i]), linestyle=ls[i], color='black', label=label[i])
    #plt.ylim([0.1E-13, 1.3E-13])

#for i in range(len(data)-1):
    #plt.semilogx(data[0], data[i+1], '-', color='red')


plt.xlabel("Bias Voltage(V)")
plt.ylabel("Capacitance(F/um^2)")

fig.set_size_inches(9, 6, forward=True)
plt.grid()
plt.legend()
#plt.savefig('hw01_tc.png', bbox_inches='tight')
plt.show()
