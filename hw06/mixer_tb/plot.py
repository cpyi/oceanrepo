import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()

arr = np.loadtxt("mixer_tran.data", skiprows=2)
x = np.transpose(arr)[0]
y1 = np.transpose(arr)[1]
y2 = np.transpose(arr)[2]
plt.plot(x, y1, linestyle='dashed', color='black', label='Mixer+Adder Output')
plt.plot(x, y2, '-', color='black', label='Mixer Output')

plt.xlabel("Time(S)")
plt.ylabel("Output Voltage(V)")
plt.xlim(x[0], x[-1])
plt.legend()

fig.set_size_inches(9, 6, forward=True)
plt.grid()
plt.savefig('mixer_tran.png', bbox_inches='tight')
plt.show()
