import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()

fn = ["i_ph_33x10x3.data", "z_real_33x10x3.data"]
label = ["Phase(current)", "Real(Z)"]
ls = ["solid", "dashed", "dashdot"]

for i in range(len(fn)):
    arr = np.loadtxt(fn[i], skiprows=3)
    data = np.transpose(arr)
    plt.semilogx(data[0], data[1], linestyle=ls[i], color='black', label=label[i])
    plt.ylim([0, 100])

#for i in range(len(data)-1):
    #plt.semilogx(data[0], data[i+1], '-', color='red')


plt.xlabel("Frequency(Hz)")
plt.ylabel("Phase(Degree)/Impedance(Ohm)")

fig.set_size_inches(9, 6, forward=True)
plt.grid()
plt.legend(loc=(0.6, 0.3))
#plt.savefig('hw01_tc.png', bbox_inches='tight')
plt.show()
