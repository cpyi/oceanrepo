import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()

arr_simple = np.loadtxt("simple.data", skiprows=2)
arr_target = np.loadtxt("target.data", skiprows=2)

s11_simple = np.transpose(arr_simple)[1]
s11_target = np.transpose(arr_target)[1]

s21_simple = np.transpose(arr_simple)[3]
s21_target = np.transpose(arr_target)[3]

freq = np.transpose(arr_simple)[0]

# By Pei Yi Chen

plt.plot(freq*1E-9, 20*np.log10(s11_simple), linestyle="dashed", color='black', label="Fit using three inductor model")
plt.plot(freq*1E-9, 20*np.log10(s11_target), '-', color='black', label="Target response")

plt.xlabel("Frequency(GHz)")
plt.ylabel("S11(dB)")

plt.ylim(-2, 0.2)

#x = [1.61]
#y = [1.61]
#plt.scatter(x, y, color='black')
plt.text(4.5, 0.0375, 'S11 approximately equal to 1(0dB) for the three inductor model', fontsize=13)


fig.set_size_inches(9, 6, forward=True)
plt.legend(loc=(0.4, 0.1), prop={'size':15})
plt.grid()
plt.savefig('s11.png', bbox_inches='tight')
plt.show()
