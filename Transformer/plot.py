import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('font', family='serif')
matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()

arr = np.loadtxt("zp.data", skiprows=2)

freq = np.transpose(arr)[0]

z11_real = np.transpose(arr)[1]
z11_imag = np.transpose(arr)[2]
z11 = z11_real + 1j * z11_imag

z12_real = np.transpose(arr)[3]
z12_imag = np.transpose(arr)[4]
z12 = z12_real + 1j * z12_imag

z21_real = np.transpose(arr)[5]
z21_imag = np.transpose(arr)[6]
z21 = z21_real + 1j * z21_imag

z22_real = np.transpose(arr)[7]
z22_imag = np.transpose(arr)[8]
z22 = z22_real + 1j * z22_imag

zs1 = z11 - (z12 + z21) / 2
zs2 = z22 - (z12 + z21) / 2
lm = np.interp(20E9, freq, np.imag(z12 + z21)/2) / (2*3.14159*20E9)
print("lm=" + str(lm)) 
ysub = 2 / (z12 + z21) - 1 / (1j * 2 * 3.14159 * freq * lm)
omega = 2 * 3.14159 * freq

f1 = omega * omega / np.real(ysub)
f2 = np.imag(ysub) * omega / np.real(ysub)

# By Pei Yi Chen
plt.plot(freq, np.absolute(z12), '-', color='black')
#plt.plot(freq, np.imag(zs1), '-', color='black')

ls1_approx = np.interp(1E9, freq, np.imag(zs1)) / (2*3.14159*1E9)
print("ls1~=" + str(ls1_approx))
ls2_approx = np.interp(1E9, freq, np.imag(zs2)) / (2*3.14159*1E9)
print("ls2~=" + str(ls2_approx))

plt.xlabel("Frequency(V)")
plt.ylabel("z-parameter(V)")

#x = [1.61]
#y = [1.61]
#plt.scatter(x, y, color='black')
#plt.text(1.7, 1.61, 'Bias point Vi=Vo=1.61V', fontsize=13)

fig.set_size_inches(9, 6, forward=True)
plt.grid()
#plt.savefig('dc.png', bbox_inches='tight')
plt.show()
