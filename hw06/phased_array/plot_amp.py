import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# matplotlib.rc('font', family='serif')
#matplotlib.rcParams.update({'font.size': 14})

fig = plt.figure()

arr = np.loadtxt("amp.data", skiprows=3)
plt.plot(np.transpose(arr)[0], np.transpose(arr)[1])

#arr = np.loadtxt("amp_theta1_90.data", skiprows=3)
#plt.plot(np.transpose(arr)[0], np.transpose(arr)[1])

#arr = np.loadtxt("amp_theta1_-54.data", skiprows=3)
#plt.plot(np.transpose(arr)[0], np.transpose(arr)[1])

plt.xlabel("Theta2(Degree)")
plt.ylabel("Vif(V)")

#plt.text(34, 0.5, '$\theta=theta$', fontsize=13)

#fig.set_size_inches(9, 6, forward=True)
plt.grid()
#plt.savefig('dc.png', bbox_inches='tight')
plt.show()
